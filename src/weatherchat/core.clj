(ns weatherchat.core
  (:gen-class)
    (:use weatherchat.calc)
    )

(defn -main
  "This is the entry point for this application. The entry point executes the function in calculations.
  In summary the application wants to calculate the spread for each month and then get the month with the highest spread.
  Details of the operation will be in the specific functions within the application."
  [& args]
      (get-max-spread-from-file))
