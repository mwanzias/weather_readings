(ns weatherchat.calc
  (:require [clojure.string :as str])
  (:import (java.io File)))

(declare try-parse-int read-file-data)

(defn try-parse-int
  "Neither Java no clojure has a way of checking if a string can be converted to an Integer, Hence the neccesity
  for this function to be able to try convert an string to integer, in case it is convertable then the integer value is returned
  Otherwise a nil is returned."
  [s]
  (try
    (Integer/parseInt s)
    (catch Exception e
      nil)))

(defn get-max-spread-from-file
  "This function maps the anonymous function on the vector of vectors loaded by the (read-file-data)

   validate that the maximum temperature (MxT) is greator than or equal to Minimum temperature (MnT)

   checks for each Mxt and MnT to remove the * character.
   Calculates the spread per moth making a map of month as the keyword and spread as the value.

   On completion we then find the key with maximum spread.

   use key in step above to obtain the value of the key

  "

  []
  (let [map-spread-months (into {} (map (fn [line]
                                          (let [l (first line)
                                                map-r (clojure.string/split (clojure.string/triml l)  #"\s+")]

                                            (if (> (count map-r) 3)
                                              (do
                                                (let [month (nth map-r 0)
                                                      maxread (nth map-r 1)
                                                      minread (nth map-r 2)
                                                      max-tm (try-parse-int
                                                               (if (clojure.string/includes? maxread "*")
                                                                 (clojure.string/trim (clojure.string/replace maxread #"\*" ""))
                                                                 maxread))
                                                      min-tm (try-parse-int
                                                               (if (clojure.string/includes? minread "*")
                                                                 (clojure.string/trim (clojure.string/replace minread #"\*" ""))
                                                                 minread)
                                                               )]
                                                  ; (println month  (- (Integer/parseInt max-tm) (Integer/parseInt min-tm)))
                                                  ; if max-tm is less than min-tm stop
                                                  ; the entir processing because the file is corrupt
                                                  (if (and min-tm max-tm)
                                                    (if (>= max-tm min-tm)
                                                      (let [spread (if (and (integer? max-tm) (integer? min-tm))
                                                                     (- max-tm min-tm)
                                                                     nil)]
                                                        {(keyword month) spread})
                                                      (throw (Exception. "The weather source file seems corrupt. Maxreading cannot be less than minimum reading"))
                                                      )
                                                    {})
                                                  )
                                                )
                                              )
                                            )
                                          ) (read-file-data)))]
    (let [max-key (name (key (apply max-key val map-spread-months)))
          max-value (get map-spread-months (keyword max-key))
          ]
      (println (format "%s %s" max-key max-value))
      )
    )
  )



(defn read-file-data
  "This function uses the environment variable   'weather_file'
  if the environment variable is not set the application will throw an exception, i.e the weather file has not been found.

  when the environment setting is set to a file, The function then reads the file content into a vector of vectors [[_ _][_ _][_ _][_ _]]
  "
  []
  (let [result (let [result (System/getenv "weather_file")]
                 (when result (File. result)))]
    (if (and result (.isFile result))
      (into [] (map (fn [liner]
                      [liner])
                    (clojure.string/split-lines  (slurp result))))
      (throw (Exception. (format "weather file is not not found. it is %s" result)))))
  )