**Requirements**
--------------------

The application for the technical assesment is programmed using Clojure. 

So the application runs on JVM and will require JDK and Jre, version 1.8 works


**How it works.**

1. The application reads the content of the file into a vector of vectors

2.  using the first vector the application gets the indexes of Mnt, MxT and Dy

3. The application then splits each of the vectors into elements picking the values of the Mnt, MxT and Dy columns by their indexes

4. Computes the spread (MxT - MnT) reading it back to  a key value pair of Dy and Spread. 

5. We then find the key with maximum spread and print it to the screem


**Assumptions**



The following are the assumptions made during the analysis of the problem

1. That any of the MnT and MxT can have an * and hence for every MnT| MxT processing it is removed before computations to avoid NumbeFormatException

2. That once in a while we may recieve a file with some corrupt values for MnT and MxT this will cause the application to throw an error

3. It also assumed that the contents of the file may change but the naming of the columns should remain the same and especially for MnT, MxT, Dy for the sake of this application. 

4. the values for MnT, MxT will remain as numerical and will not be text value of the values like "seventy nine" instead of 79




To setup and run the application do the following. 

1. Create an environment variable named weather_file pointing to the weather.dat file that contains the monthly weather data. 

2. from the sources shared under the directory weatherchat/target/uberjar/ copy the file weatherchat-0.1.0-standalone.jar to your desirable location. 	

3. from the location that you have moved the file issue the following comment.  java -jar weatherchat-0.1.0-standalone.jar

The application now should print on the standard input output the month and the spread like 2 16




**Compiling** 



To Edit | recompile the sources you will need Lein installed on your machine, then when you have navigated to the directory  weatherchat issue lein uberjar, This compiles a new jar file. 

Lein can be obtained from the following link https://leiningen.org/#install